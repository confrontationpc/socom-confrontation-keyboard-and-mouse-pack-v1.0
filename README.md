![](/Images/1.png)

**SOCOM: Confrontation KEYBOARD & MOUSE PACK V1.0**

  **By S3ROMIC**

**(For BCES-00173, BCUS-98183, BCUS-98152)**

This guide will help you set up the mouse and keyboard pack for SOCOM Confrontation on 

The RPCS3 Emulator.

Please note that this is in early Development of this project you may encounter Issues like

**Medium Mouse Movement**

**Unable To Walk Slow**

**And Jumpy Mouse Movement**

[**DOWNLOAD**](https://drive.google.com/file/d/1wdUHp_a8_TCaV71C3b8Jcth-FaScLgz3/view?usp=drivesdk)

First, make sure you have Confrontation installed and working properly, your game should be updated to 1.61 and you should have at least logged into the server and gotten to the main online screen to make sure your game works properly before you follow this guide.

Step 1.) Open RPCS3 and right click your SOCOM: U.S. Navy SEALs Confrontation and look for “Open Custom Config Folder”

![](/Images/2.png)


` `Step 2.) Once In The “custom\_configs” you want to click on the top bars “config”

![](/Images/3.png)

Step 3.) Once Out And In To The “config” Folder you want to locate “input\_configs”

![](/Images/4.png)




Step 4.) Find what game serial matches your game serial and drag it from the Confrontation KEYBOARD & MOUSE PACK into the “input\_configs” and restart RPCS3

![](/Images/5.png)





















Step 5.) Once RPCS3 Opens left click Configuration and click emulator and make sure 

“Ignore doubleclicks for fullscreen” is checked

![](/Images/6.png)

Step 6.) Launch SOCOM: Confrontation Login to the main menu go to Options and wireless controller and general options and turn Look Speed and Acceleration to the max 

![](/Images/7.png)

Step 7.)  Go to control schemes and select Default  

![](/Images/8.png)














And You are Finished the SOCOM: Confrontation KEYBOARD & MOUSE PACK go have fun playing Confrontation

![](/Images/9.png)
